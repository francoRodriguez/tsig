package com.tsig.project.tsigproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TsigProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(TsigProjectApplication.class, args);
	}

}
